package taller.mundo;

import java.util.ArrayList;

public class LineaCuatro 
{	
	private final static String SIMBOLO_MAQUINA="%";
	
	/**
	 * Número de filas del tablero
	 */
	private int tamanioXTab;
	
	/**
	 * Número de columnas del tablero.
	 */
	private int tamanioYTab;
	
	/**
	 * Matriz que representa el estado actual del juego.
	 */
	private String[][] tablero;
	
	/**
	 * Lista que contiene a los jugadores
	 */
	private ArrayList<Jugador> jugadores;
	
	/**
	 * Nombre del jugador en turno
	 */
	private String atacante;
	
	/**
	 * Determina si el juego se terminó
	 */
	private boolean finJuego;
	
	/**
	 * El número del jugador en turno
	 */
	private int turno;
	
	/**
	 * Crea una nueva instancia del juego
	 */
	public LineaCuatro(ArrayList <Jugador> pJugadores,int pFil, int pCol)
	{
		tamanioXTab = pFil;
		tamanioYTab = pCol;
		tablero = new String[pFil][pCol];
		for(int i=0;i<tablero.length;i++)
		{
			for(int j=0;j<tablero[0].length;j++)
			{
				tablero[i][j]="___";
			}

		}
		jugadores = pJugadores;
		finJuego = false;
		turno = 0;
		atacante = jugadores.get(turno).darNombre();
	}
	/**
	 * Retorna el tablero del juego
	 * @return tablero
	 */
	public String[][] darTablero()
	{
		return tablero;
	}
	
	/**
	 * Retorna el jugador en turno
	 * @return atacante jugador en turno
	 */
	public String darAtacante()
	{
		return atacante;
	}
	/**
	 * Determina si el juego termina
	 * @return true si el juego se termino, false de lo contrario
	 */
	public boolean fin()
	{
		return finJuego;
	}
	
	/**
	 * Registra una jugada aleatoria
	 */
	public void registrarJugadaAleatoria()
	{
		//TODO
		int fila = tamanioXTab-1;
		boolean marco = false;
		for(int j = 0; j < tamanioXTab && !marco; j++)
		{
			while(fila >= 0)
			{
				if(tablero[fila][j].equals(""))
				{
					marco = true;
					tablero[fila][j] = SIMBOLO_MAQUINA;
				}
				fila--;
			}	
			fila = tamanioXTab-1;
		}
	}
	
	/**
	 * Registra una jugada en el tablero
	 * @return true si la jugada se pudo realizar, false de lo contrario
	 */
	public boolean registrarJugada(int col)
	{
		//TODO
		Jugador actual = jugadores.get(turno);
		atacante = actual.darNombre();
		boolean marco = false;
		for(int i = 6; i >= 0 && !marco; i--)
		{
			if(tablero[i][col].equals(""))
			{
				marco = true;
				tablero[i][col] = actual.darSimbolo();
			}
				
		}
		return marco;
	}
	
	/**
	 * Determina si una jugada causa el fin del juego
	 * @return true si la jugada termina el juego false de lo contrario
	 */
	public boolean terminar(int fil,int col)
	{
		//TODO
		return true;
	}


}
